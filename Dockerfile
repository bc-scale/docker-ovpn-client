FROM golang:latest as build

WORKDIR /app

COPY go.mod go.sum /app/
COPY vendor /app/
COPY . /app/

RUN go build -o /bin/myproxy cmd/myproxy/main.go


FROM alpine:latest
COPY ./services/ /services/
COPY entry.sh /usr/local/bin/
COPY healthcheck.sh /usr/local/bin/
COPY --from=build /bin/myproxy /usr/local/bin/

RUN apk --no-cache --no-progress add \
        bash \
        ca-certificates \
        curl \
        ip6tables \
        iptables \
        openvpn \
        tzdata && \
    apk add --update --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ daemontools && \
    rm  -rf /tmp/* /var/cache/apk/*

VOLUME ["/vpn/config.ovpn", "/vpn/auth.txt"]
EXPOSE 8118

HEALTHCHECK --interval=20s --timeout=15s --start-period=90s --retries=3 CMD [ "/usr/local/bin/healthcheck.sh" ]

ENTRYPOINT ["/usr/local/bin/entry.sh"]
